# Cloud_Project_3
By Jiayi Zhou 
[![pipeline status](https://gitlab.com/JiayiZhou36/cloud_project_3/badges/master/pipeline.svg)](https://gitlab.com/JiayiZhou36/cloud_project_3/-/commits/master)

## Purpose of Project
This project creates an S3 Bucket using CDK with AWS CodeWhisperer.

## Requirements
* Create S3 bucket using AWS CDK
* Use CodeWhisper to generate CDK code
* Add bucket properties like versioning and encryption


## CodeWhisper
To generate CDK (Cloud Development Kit) code for creating an S3 bucket with encryption and versioning using CodeWhisper, I initiated the process by providing a prompt outlining the task I aimed to complete. This task typically involves specifying requirements such as building an S3 bucket with encryption and versioning enabled. Following this, CodeWhisper processes the provided prompt, taking a short duration to generate the necessary code.

Once the processing is complete, CodeWhisper furnishes me with the code tailored to my requirements, assisting me in leveraging CDK to develop the S3 bucket. This streamlined approach allows for efficient utilization of CDK resources, facilitating the creation of S3 buckets with desired configurations, such as encryption and versioning, seamlessly.

## S3 Bucket
![Screenshot_2024-02-10_at_5.10.34_PM](/uploads/ad68d8a5ff7c5df6165ec34d9a870898/Screenshot_2024-02-10_at_5.10.34_PM.png)

## S3 Bucket Version
![Screenshot_2024-02-10_at_5.26.05_PM](/uploads/34903d4cb20a41801263b3872be058fc/Screenshot_2024-02-10_at_5.26.05_PM.png)

## S3 Encryption
![Screenshot_2024-02-10_at_5.26.19_PM](/uploads/4b49d462482c171f7823d15409ced39e/Screenshot_2024-02-10_at_5.26.19_PM.png)


