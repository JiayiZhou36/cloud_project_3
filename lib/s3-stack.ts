import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as iam from 'aws-cdk-lib/aws-iam';
// import * as sqs from 'aws-cdk-lib/aws-sqs';
// make an s3 bucket
  
export class S3Stack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);
  
    // make an s3 bucket and add bucket properties like versioning and encryption
    const bucket = new cdk.aws_s3.Bucket(this, 'jiayizhou-mini3', {
      versioned: true,
      encryption: cdk.aws_s3.BucketEncryption.S3_MANAGED,
      removalPolicy: cdk.RemovalPolicy.DESTROY,
    });
    
    bucket.grantRead(new iam.AccountRootPrincipal());

  }
}

